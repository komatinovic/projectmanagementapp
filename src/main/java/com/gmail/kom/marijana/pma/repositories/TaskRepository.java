package com.gmail.kom.marijana.pma.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gmail.kom.marijana.pma.entities.Project;
import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;

public interface TaskRepository extends CrudRepository<Task, Long>{

	List<Task> findByAssignee(User user);
	List<Task> findByProject(Project project);
	
}
