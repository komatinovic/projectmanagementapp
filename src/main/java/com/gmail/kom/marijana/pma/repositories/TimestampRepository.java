package com.gmail.kom.marijana.pma.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gmail.kom.marijana.pma.entities.UserTimestamp;

public interface TimestampRepository extends CrudRepository<UserTimestamp, Long>{

	@Query("SELECT ut FROM UserTimestamp ut WHERE ut.tag = :tag ORDER BY ut.id DESC")
	List<UserTimestamp> findLastByTag(@Param("tag") String tag);
	
	UserTimestamp findFirstByTagOrderByIdDesc(String tag);
}
