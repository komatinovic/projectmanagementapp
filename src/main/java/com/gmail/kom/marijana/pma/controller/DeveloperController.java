package com.gmail.kom.marijana.pma.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;
import com.gmail.kom.marijana.pma.repositories.TaskRepository;
import com.gmail.kom.marijana.pma.repositories.UserRepository;

@Controller
@RequestMapping ("/developer")
public class DeveloperController {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@GetMapping("/")
	public String developer(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User developer = userRepository.findByUsername(username);
		
		model.addAttribute("tasks", taskRepository.findByAssignee(developer));

		List<Task> unAssTasks = new ArrayList<>();
		for(Task task : taskRepository.findAll())
			if(task.getAssignee() == null)
				unAssTasks.add(task);

		model.addAttribute("unass_tasks", unAssTasks);

		return "developer";
	}
	
	@GetMapping ("/tasks")
	public String tasks(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User developer = userRepository.findByUsername(username);
		
		model.addAttribute("tasks", taskRepository.findByAssignee(developer));
		
		return "developer-tasks";
	}
	
	@GetMapping("/task/{id}")
	public String task(@PathVariable Long id, Model model){
		
		model.addAttribute("task", taskRepository.findOne(id));
		
		return "developer-task";
	}
	
	@PostMapping("/editTask")
	public String editTask(Task task){
		
		taskRepository.save(task);
		
		return "redirect:/developer/tasks";
	}
}
