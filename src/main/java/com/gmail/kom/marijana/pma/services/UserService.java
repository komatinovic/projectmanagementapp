package com.gmail.kom.marijana.pma.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;
import com.gmail.kom.marijana.pma.repositories.TaskRepository;
import com.gmail.kom.marijana.pma.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	public void deleteUser(Long id){
		
		// get user
		User user = userRepository.findOne(id);
		
		// find all tasks where user is assignee
		List<Task> tasks = taskRepository.findByAssignee(user);
		
		// remove user from tasks
		for(Task task : tasks){
			task.setAssignee(null);
			taskRepository.save(task);
		}
		
		// delete user
		userRepository.delete(user);
	}
}
