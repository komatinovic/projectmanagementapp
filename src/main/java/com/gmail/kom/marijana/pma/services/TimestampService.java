package com.gmail.kom.marijana.pma.services;

import java.util.Date;
import java.util.List;

import javax.jws.soap.SOAPBinding.Use;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gmail.kom.marijana.pma.entities.User;
import com.gmail.kom.marijana.pma.entities.UserTimestamp;
import com.gmail.kom.marijana.pma.repositories.TimestampRepository;
import com.gmail.kom.marijana.pma.repositories.UserRepository;

@Service
public class TimestampService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TimestampRepository timestampRepository;
	
	@Transactional
	public UserTimestamp addTimestampToUser(String tag){
		
		UserTimestamp lastTimestamp = timestampRepository.findFirstByTagOrderByIdDesc(tag);
		
		UserTimestamp newTimestamp = new UserTimestamp();
		newTimestamp.setTag(tag);
		newTimestamp.setDate(new Date());
		newTimestamp.setInOut(!lastTimestamp.isInOut());
		newTimestamp.setUser(lastTimestamp.getUser());
		
		User user = userRepository.findByIdWithTags(lastTimestamp.getUser().getId());
		
		user.getTimestampList().add(newTimestamp);
		
		userRepository.save(user);
		
		return lastTimestamp;
	}
	
}
