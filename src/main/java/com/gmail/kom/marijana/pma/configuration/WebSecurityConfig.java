package com.gmail.kom.marijana.pma.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	DataSource dataSource;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/css/**", "/js/**", "/images/**", "/sb-admin/**").permitAll()
				.antMatchers("admin", "admin/**").access("hasRole('ROLE_ADMIN')")
				.antMatchers("developer", "developer/**").access("hasRole('ROLE_DEVELOPER')")
				.antMatchers("manager", "manager/**").access("hasRole('ROLE_MANAGER')")
				.antMatchers("/create").permitAll()
				.antMatchers("/timestamp/add").permitAll()
	            .anyRequest().authenticated()
	            .and()
            .formLogin()
            	.loginPage("/login")
	            .permitAll()
	            .and()
	        .logout()
		        .logoutRequestMatcher(new AntPathRequestMatcher("/logout")) 
	            .logoutSuccessUrl("/") 
	            .permitAll();
	}

	@Autowired
	public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception{
		
		auth
			.jdbcAuthentication()
	    	.dataSource(dataSource)
	    	.usersByUsernameQuery("SELECT username, password, enabled FROM users WHERE username=?")
	    	.authoritiesByUsernameQuery("SELECT username, roles FROM users WHERE username=?");
	}
	
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("ADMIN");
    }
	
}
