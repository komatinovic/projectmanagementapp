package com.gmail.kom.marijana.pma.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.gmail.kom.marijana.pma.entities.Project;
import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;

public interface ProjectRepository extends CrudRepository<Project, Long>{

	List<Project> findByManager(User user);
	
}
