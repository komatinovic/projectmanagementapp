package com.gmail.kom.marijana.pma.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "userTimestamp")
public class UserTimestamp {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@DateTimeFormat(pattern = "hh:mm:ss")
	private Date date;
	
	private boolean inOut;
	private String tag;
	
	@JsonBackReference
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "user_id")
	private User user;

	public UserTimestamp() {
		super();
	}

	public UserTimestamp(Long id, Date date, boolean inOut, String tag, User user) {
		super();
		this.id = id;
		this.date = date;
		this.inOut = inOut;
		this.tag = tag;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isInOut() {
		return inOut;
	}

	public void setInOut(boolean inOut) {
		this.inOut = inOut;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
