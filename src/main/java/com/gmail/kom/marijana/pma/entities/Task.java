package com.gmail.kom.marijana.pma.entities;





import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "task")
public class Task {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private User assignee;
	
	private String status;
	
	private Double progress;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date deadline;
	private String description;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "project_id")
	private Project project;
	
	public Task() {
		super();
	}
	public Task(User assignee, double progress, Date deadline, String description) {
		super();
		this.assignee = assignee;
		this.progress = progress;
		this.deadline = deadline;
		this.description = description;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public User getAssignee() {
		return assignee;
	}
	public void setAssignee(User assignee) {
		this.assignee = assignee;
	}
	public double getProgress() {
		return progress;
	}
	public void setProgress(double progress) {
		this.progress = progress;
	}
	public Date getDeadline() {
		return deadline;
	}
	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	
}
