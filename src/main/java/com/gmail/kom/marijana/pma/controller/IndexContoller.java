package com.gmail.kom.marijana.pma.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gmail.kom.marijana.pma.entities.Project;
import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;
import com.gmail.kom.marijana.pma.repositories.ProjectRepository;
import com.gmail.kom.marijana.pma.repositories.TaskRepository;
import com.gmail.kom.marijana.pma.repositories.UserRepository;

@Controller
@RequestMapping("/")
public class IndexContoller {

	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping("/")
	public String home(HttpServletRequest request, Model model){
		
		if(request.isUserInRole("ROLE_ADMIN")){
			return "redirect:/admin/";
		}
		else if(request.isUserInRole("ROLE_DEVELOPER")){
			return "redirect:/developer/";
		}
		else if(request.isUserInRole("ROLE_MANAGER")){
			return "redirect:/manager/";
		}
		
		return "login";
	}
	
	@RequestMapping("/login")
	public String login(Model model){
		return "login";
	}
	
	@GetMapping("/create")
	@ResponseBody
	public String create(String username, String password){
		
		User user = new User(username, password);
		
		try{
			userRepository.save(user);
		} catch(Exception ex){
			return "Error creating new user." + ex.toString();
		}
			
		return "New user is created, id: " + user.getId();	
	}
	
}
