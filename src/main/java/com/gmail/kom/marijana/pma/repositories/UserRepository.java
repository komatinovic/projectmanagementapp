package com.gmail.kom.marijana.pma.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gmail.kom.marijana.pma.entities.User;

public interface UserRepository extends CrudRepository<User, Long>{

	public User findByUsername(String username);
	public List<User> findByRolesContaining(String role);
	
	@Query("SELECT u FROM User u LEFT JOIN FETCH u.timestampList WHERE u.id = :id")
	public User findByIdWithTags(@Param("id") long id);

	@Query("SELECT DISTINCT(u) FROM User u LEFT JOIN FETCH u.timestampList")
	public List<User> findAllWithTags();
}
