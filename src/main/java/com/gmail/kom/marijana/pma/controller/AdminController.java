package com.gmail.kom.marijana.pma.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.gmail.kom.marijana.pma.entities.Project;
import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;
import com.gmail.kom.marijana.pma.entities.UserTimestamp;
import com.gmail.kom.marijana.pma.repositories.ProjectRepository;
import com.gmail.kom.marijana.pma.repositories.TaskRepository;
import com.gmail.kom.marijana.pma.repositories.TimestampRepository;
import com.gmail.kom.marijana.pma.repositories.UserRepository;
import com.gmail.kom.marijana.pma.services.TaskService;
import com.gmail.kom.marijana.pma.services.UserService;

@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserService userService;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@Autowired
	private TimestampRepository timestampRepository;

	@GetMapping("/")
	public String admin(Model model){
		model.addAttribute("projects", projectRepository.findAll());
		model.addAttribute("users", userRepository.findAll());
		model.addAttribute("tasks", taskRepository.findAll());
		return "admin";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder){
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	//timestamps
	@GetMapping("/timestamps")
	public String timestamps(Model model){
		
		List<User> allUsers = userRepository.findAllWithTags();
		List<User> inUsers = new ArrayList<>();
		
		for(User user : allUsers)
			if(user.getTimestampList().get(user.getTimestampList().size()-1).isInOut())
				inUsers.add(user);
		
		model.addAttribute("users", inUsers);
		
		return "admin-timestamps";
	}
	
	//users
	@GetMapping("/users")
	public String users(Model model){
		model.addAttribute("users", userRepository.findAll());
		return "admin-users";
	}
	
	@GetMapping("/user/{id}")
	public String user(@PathVariable Long id, Model model){
		
		if(id==null)
			return "redirect:/admin";
		
		User user = userRepository.findOne(id);
		
		model.addAttribute("user", user);
		model.addAttribute("tasks", taskRepository.findByAssignee(user));
		
		return "admin-user";
	}

	@GetMapping("/users/new")
	public String usersNew(){
		return "admin-add-user";
	}
	
	@PostMapping("/deleteUser")
	public String deleteUser(@RequestParam Long id){
		
		userService.deleteUser(id);
		
		return "redirect:/admin/users";
	}

	@PostMapping("/editUser")
	public String editUser(User user){
		
		userRepository.save(user);
		
		return "redirect:/admin/users";
	}

	@PostMapping("/addUser")
	public String addUser(User user){
		
		user.setEnabled(true);
		
		UserTimestamp timestamp = new UserTimestamp();
		timestamp.setTag(user.getTag());
		timestamp.setDate(new Date());
		timestamp.setUser(user);
		timestamp.setInOut(true);
		
		user.getTimestampList().add(timestamp);
		
		userRepository.save(user);
		
		return "redirect:/admin/users";
	}
	
	//projects
	@GetMapping("/projects")
	public String projects(Model model){
		model.addAttribute("projects", projectRepository.findAll());
		return "admin-projects";
	}
	
	@GetMapping("/project/{id}")
	public String project(@PathVariable Long id, Model model){
		
		if(id==null)
			return "redirect:/admin";
		
		Project project = projectRepository.findOne(id);
		
		model.addAttribute("project", project);
		model.addAttribute("users", userRepository.findAll());
		model.addAttribute("tasks", taskRepository.findByProject(project));
		
		return "admin-project";
	}
	
	@PostMapping("/deleteProject")
	public String deleteProject(@RequestParam Long id){
		
		projectRepository.delete(id);
		
		return "redirect:/admin/projects";
	}
	
	@PostMapping("/editProject")
	public String editProject(Project project){
		
		projectRepository.save(project);
		
		return "redirect:/admin/projects";
	}
	
	@GetMapping("/projects/new")
	public String projectsNew(Model model){

		List<User> managers = userRepository.findByRolesContaining("ROLE_MANAGER");
		
		model.addAttribute("users", managers);
		
		return "admin-add-project";
	}
	
	@PostMapping("/addProject")
	public String addProject(Project project){
		
		projectRepository.save(project);
		
		return "redirect:/admin/projects";
	}
	
	//tasks
	@GetMapping("/tasks")
	public String tasks(Model model){
		model.addAttribute("tasks", taskRepository.findAll());
		model.addAttribute("projects", projectRepository.findAll());
		return "admin-tasks";
	}
	
	@PostMapping("/addTask")
	public String addTask(Task task){
		
		if(task.getAssignee().getId() == 0)
			task.setAssignee(null);
		
		taskRepository.save(task);
		
		return "redirect:/admin/tasks";
	}
	
	@GetMapping("/tasks/new")
	public String tasksNew(Model model){
		model.addAttribute("users", userRepository.findAll());
		model.addAttribute("projects", projectRepository.findAll());
		return "admin-add-task";
	}
	
	@PostMapping("/deleteTask")
	public String deleteTask(@RequestParam Long id){
		
		taskRepository.delete(id);
		
		return "redirect:/admin/tasks";
	}
	
	@GetMapping("/task/{id}")
	public String task(@PathVariable Long id, Model model){
		
		if(id==null)
			return "redirect:/admin";
		
		Task task = taskRepository.findOne(id);
		
		model.addAttribute("task", task);
		model.addAttribute("users", userRepository.findAll());
		model.addAttribute("projects", projectRepository.findAll());
		
		return "admin-task";
	}
	
	@PostMapping("/editTask")
	public String editTask(Task task){
		
		taskRepository.save(task);
		
		return "redirect:/admin/tasks";
	}
}
