package com.gmail.kom.marijana.pma.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gmail.kom.marijana.pma.entities.UserTimestamp;
import com.gmail.kom.marijana.pma.repositories.TimestampRepository;
import com.gmail.kom.marijana.pma.services.TimestampService;

@RestController
@RequestMapping("/timestamp")
public class TimestampController {

	@Autowired
	TimestampService timestampService;
	
	@Autowired
	TimestampRepository timestampRepository;
	
	@GetMapping("/add")
	public UserTimestamp checkin(@RequestParam String tag){
		
		return timestampService.addTimestampToUser(tag);
	}
	
}
