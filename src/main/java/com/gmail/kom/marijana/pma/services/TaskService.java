package com.gmail.kom.marijana.pma.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gmail.kom.marijana.pma.entities.Project;
import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.repositories.ProjectRepository;
import com.gmail.kom.marijana.pma.repositories.TaskRepository;

@Service
public class TaskService {

	@Autowired
	private TaskRepository taskRepository;
	
	@Autowired
	private ProjectRepository projectRepository;
	
	public void addTaskToProjectWithId(Task task, Long id){
		
		if(id==null || task==null)
			return;
		
		Project project = projectRepository.findOne(id);
		
		project.getTaskList().add(task);
		
		projectRepository.save(project);
	}
	
}
