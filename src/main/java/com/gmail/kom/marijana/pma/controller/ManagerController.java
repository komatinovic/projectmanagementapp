package com.gmail.kom.marijana.pma.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gmail.kom.marijana.pma.entities.Project;
import com.gmail.kom.marijana.pma.entities.Task;
import com.gmail.kom.marijana.pma.entities.User;
import com.gmail.kom.marijana.pma.repositories.ProjectRepository;
import com.gmail.kom.marijana.pma.repositories.TaskRepository;
import com.gmail.kom.marijana.pma.repositories.UserRepository;

@Controller
@RequestMapping("/manager")
public class ManagerController {

	@Autowired
	private ProjectRepository projectRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TaskRepository taskRepository;
	
	@GetMapping("/")
	public String manager(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User manager = userRepository.findByUsername(username);
		
		model.addAttribute("projects", projectRepository.findByManager(manager));
		model.addAttribute("manager", userRepository.findOne(manager.getId()));
		return "manager";
	}
	
	//projects
	@GetMapping("/projects")
	public String projects(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User manager = userRepository.findByUsername(username);
		
		model.addAttribute("projects", projectRepository.findByManager(manager));
		model.addAttribute("manager", userRepository.findOne(manager.getId()));
		return "manager-projects";
	}
	
	@GetMapping("/projects/new")
	public String projectsNew(Model model){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
		
		model.addAttribute("manager", userRepository.findByUsername(username));
		
		return "manager-add-project";
	}
	
	@PostMapping("/addProject")
	public String addProject(Project project){
		
		projectRepository.save(project);
		
		return "redirect:/manager/projects";
	}
	
	//tasks
	@GetMapping("/tasks")
	public String tasks(Model model){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User manager = userRepository.findByUsername(username);
	    
		List<Project> projects = projectRepository.findByManager(manager);
		model.addAttribute("projects", projects);
		
		List<Task> tasks = new ArrayList<>();
		
		for (Project project : projects) {
			 List<Task> taskovi = taskRepository.findByProject(project);
			 for(Task task : taskovi){
				 tasks.add(task);
			 }
		}
		model.addAttribute("tasks", tasks);
		
		return "manager-tasks";
	}
	
	@GetMapping("/tasks/new")
	public String tasksNew(Model model){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
		
	    User manager = userRepository.findByUsername(username);
		model.addAttribute("projects", projectRepository.findByManager(manager));

		List<User> developers = userRepository.findByRolesContaining("ROLE_DEVELOPER");
		
		model.addAttribute("users", developers);
		
		return "manager-add-task";
	}
	
	@PostMapping("/addTask")
	public String addTask(Task task){
		taskRepository.save(task);
		return "redirect:/manager/tasks";
	}
	
	@GetMapping("/task/{id}")
	public String task(@PathVariable Long id, Model model){
		
		if(id==null)
			return "redirect:/manager";
		
		Task task = taskRepository.findOne(id);
		
		model.addAttribute("task", task);
		model.addAttribute("users", userRepository.findAll());
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    String username = auth.getName();
	    User manager = userRepository.findByUsername(username);
	    
		List<Project> projects = projectRepository.findByManager(manager);
		model.addAttribute("projects", projects);
		
		return "manager-task";
	}
	
	@PostMapping("/editTask")
	public String editTask(Task task){
		
		taskRepository.save(task);
		
		return "redirect:/manager/tasks";
	}
}
