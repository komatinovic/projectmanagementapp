package com.gmail.kom.marijana.pma.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
	
	private String name;
	
	@OneToMany (fetch=FetchType.EAGER, 
			cascade=CascadeType.ALL, 
			mappedBy="project")
	private List<Task> taskList = new ArrayList<>();
	
	@ManyToOne (fetch=FetchType.EAGER)
	private User manager;
	
	public Project() {
		super();
	}
	public Project(String name, User manager) {
		super();
		this.name = name;
		this.manager = manager;
	}
	public List<Task> getTaskList() {
		return taskList;
	}
	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public User getManager() {
		return manager;
	}
	public void setManager(User manager) {
		this.manager = manager;
	}
	@Override
	public String toString() {
		return name;
	}
	
}
